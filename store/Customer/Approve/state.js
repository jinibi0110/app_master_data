export default {
    customerApproveMainTableData: [{
        U_REQUEST_CODE: "001",
        U_REQUEST_TYPE: "New",
        U_CUSTOMER_NAME: "Juan Dela Cruz",
        U_DATE_REQUESTED: "04-MAR-2020",
        U_REQUESTED_BY: "Kiela Gee",
        U_REQUESTED_BY_ID: 1,
        U_DATE_APPROVED: "",
        U_APPROVED_BY: "",
        U_APPROVED_BY_ID: "",
        U_STATUS: "Requested"
    }],

    viewCustomerDetailsData: [{
        U_CUS_DETAILS_CODE: 1,
        U_COMPANY_NAME: "Biotech Farms Incorporated",
        U_BP_TYPE: "Customer",
        U_REQUEST_TYPE: "New",
        U_CUSTOMER_TYPE: "Employee",
        U_CUSTOMER_NAME: "Juan Dela Cruz",
        U_ADDRESS: "Prk.6, Luwalhati Village, Brgy. Mabuhay, GSC",
        U_CONTACT_NO: "09129291002",
        U_PAYMENT_TERMS: "30per",
        U_PURPOSE: "Bill To",
        U_STATUS: "Requested"
    }],

    approveCustomerRequestData: [{
        U_CUS_DETAILS_CODE: 1,
        U_COMPANY_NAME: "Biotech Farms Incorporated",
        U_BP_TYPE: "Customer",
        U_REQUEST_TYPE: "New",
        U_CUSTOMER_TYPE: "Employee",
        U_CUSTOMER_NAME: "Juan Dela Cruz",
        U_ADDRESS: "Prk.6, Luwalhati Village, Brgy. Mabuhay, GSC",
        U_CONTACT_NO: "09129291002",
        U_PAYMENT_TERMS: "30per",
        U_PURPOSE: "Bill To",
        U_DATE_APPROVED: "05-MAR-2020",
        U_APPROVED_BY: "Genevie Mindajao",
        U_APPROVED_BY_ID: 2,
        U_STATUS: "Approved"
    }]
}