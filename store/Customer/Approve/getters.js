export default {
    getCustomerApproveMainTableData(state){
        return state.customerApproveMainTableData
    },

    getViewCustomerDetailsData(state){
        return state.viewCustomerDetailsData
    },

    getApproveCustomerRequestData(state){
        return state.approveCustomerRequestData
    },
}