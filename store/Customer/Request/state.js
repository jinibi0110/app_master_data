export default {

    customerList: [{
        U_CUSTOMER_CODE: 1,
        U_REQUEST_NO: "001",
        U_COMPANY_NAME: "Biotech Farms Incorporated",
        U_BP_TYPE: "Customers",
        U_REQUEST_TYPE: "New",
        U_CUSTOMER_TYPE: "Employee",
        U_CUSTOMER_NAME: "Juan Dela Cruz",
        U_ADDRESS: "Prk.6, Luwalhati Village, Brgy. Mabuhay, GSC",
        U_CONTACT_NO: "09106564656",
        U_PAYMENT_TERMS: "240NET",
        U_PURPOSE: "Bill To",
        U_DATE_REQUESTED: "09 Mar, 2020",
        U_REQUESTED_BY: "Trevor Bach",
        U_REQUESTED_BY_ID: 1,
        U_DATE_UPDATED: "",
        U_UPDATED_BY: "",
        U_UPDATED_BY_ID: "",
        U_STATUS: "Pending"
    }],
}