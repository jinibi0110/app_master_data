let id = 1;

export default {
    createCustomerRequest(state, customerData) {
        console.log(customerData)
        console.log(state.customerList);
        id++;
        customerData.U_CUSTOMER_CODE = id;
        state.customerList.unshift(customerData);
    },

    updateCustomerRequest(state, customerData) {
        const index = state.customerList.findIndex(
            customers => customers.U_CUSTOMER_CODE == customerData.U_CUSTOMER_CODE
        );

        state.customerList.splice(index, 1);
        state.customerList.push(customerData);
    },

    approveCustomerRequest(state, customerData) {
        const index = state.customerList.findIndex(
            customers => customers.U_CUSTOMER_CODE == customerData.U_CUSTOMER_CODE
        );

        state.customerList.splice(index, 1);
        state.customerList.push(customerData);
    },

    processCustomerRequest(state, customerData) {
        const index = state.customerList.findIndex(
            customers => customers.U_CUSTOMER_CODE == customerData.U_CUSTOMER_CODE
        );

        state.customerList.splice(index, 1);
        state.customerList.push(customerData);
    }
};