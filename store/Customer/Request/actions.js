export default {
    createCustomerRequest({ commit }, { customerData }) {
        commit("createCustomerRequest", customerData);

        return customerData;
    },

    updateCustomerRequest({ commit }, { customerData }) {
        commit("updateCustomerRequest", customerData);

        return customerData;
    },

    approveCustomerRequest({ commit }, { customerData }) {
        commit("approveCustomerRequest", customerData);

        return customerData;
    },

    processCustomerRequest({ commit }, { customerData }) {
        commit("processCustomerRequest", customerData);

        return customerData;
    }
}