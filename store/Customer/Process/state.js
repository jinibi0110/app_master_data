export default {

    viewCustomerProcessMainTableData: [{
        U_REQUEST_CODE:"001",
        U_REQUEST_TYPE:"New",
        U_CUSTOMER_NAME:"Juan Dela Cruz",
        U_DATE_REQUESTED:"04-MAR-2020",
        U_REQUESTED_BY:"Kiela Gee",
        U_REQUESTED_BY_ID:"1",
        U_DATE_APPROVED:"05-MAR-2020",
        U_APPROVED_BY:"Genevie Mindajao",
        U_APPROVED_BY_ID:"2",
        U_DATE_PROCESSED:"",
        U_PROCESSED_BY:"",
        U_PROCESSED_BY_ID:"",
        U_STATUS:"Approved"
    }],

    viewCustomerDetailsData: [{
        U_CUS_DETAILS_CODE:"",
        U_COMPANY_NAME:"",
        U_BP_TYPE:"",
        U_REQUEST_TYPE:"",
        U_CUSTOMER_TYPE:"",
        U_CUSTOMER_NAME:"",
        U_ADDRESS:"",
        U_CONTACT_NO:"",
        U_PAYMENT_TERMS:"",
        U_PURPOSE:"",
        U_STATUS:""
    }],

    processCustomerRequestData: [{
        U_PROCESS_CUS_CODE:"",
        U_COMPANY_NAME:"",
        U_BP_TYPE:"",
        U_REQUEST_TYPE:"",
        U_CUSTOMER_TYPE:"",
        U_CUSTOMER_NAME:"",
        U_ADDRESS:"",
        U_CONTACT_NO:"",
        U_PAYMENT_TERMS:"",
        U_PURPOSE:"",
        U_DATE_PROCESSED:"",
        U_PROCESSED_BY:"",
        U_PROCESSED_BY_ID:"",
        U_STATUS:""
    }]
}