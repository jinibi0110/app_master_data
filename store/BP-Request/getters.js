export default {
  getPendingList(state) {
    return state.pendingList;
  },

  getProcessedList(state) {
    return state.processedList;
  },
  getApprovedList(state) {
    return state.approvedList;
  }
};
