export default {
  setPendingList(state, data) {
    state.pendingList = data;
  },

  setProcessedList(state, data) {
    state.processedList = data;
  },
  addPending(state, requestData) {
    console.log(requestData);
    state.pendingList.push(requestData);
  },

  editPending(state, requestData) {
    const index = state.pendingList.findIndex(
      request => request.DocEntry === requestData.DocEntry
    );
    state.pendingList.splice(index, 1);
    state.pendingList.push(requestData);
  },
  setApprovedList(state, data) {
    state.approvedList = data;
  },
  approveRequest(state, data) {
    state.approvedList.unshift(data);
    const index = state.pendingList.findIndex(
      request => request.DocEntry === data.DocEntry
    );
    state.pendingList.splice(index, 1);
  },
  processRequest(state, data, Type) {
    console.log(data);
    if (data.RequestDetails[0].U_CARD_TYPE == "S") {
      const index = state.pendingList.findIndex(
        request => request.DocEntry === data.DocEntry
      );
      state.pendingList.splice(index, 1);
      state.processedList.push(data);
    } else if (data.RequestDetails[0].U_CARD_TYPE == "C") {
      const index = state.approvedList.findIndex(
        request => request.DocEntry === data.DocEntry
      );
      state.approvedList.splice(index, 1);
      state.processedList.push(data);
    }
  }
};
