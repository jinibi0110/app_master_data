import axios from "axios";
import moment from "moment";
export default {
  async fetchPendingList({ commit }, { SessionId, user_actions }) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/api/bp_requests/pending`,
      headers: {
        Authorization: SessionId
      },
      data: {
        user_actions
      }
    })
      .then(res => {
        if (Array.isArray(res.data.pending))
          commit("setPendingList", res.data.pending);
        else commit("setPendingList", []);
        return res;
      })
      .catch(err => err);
  },

  async fetchProcessedList({ commit }, { user_actions, SessionId }) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/api/bp_requests/processed`,
      headers: {
        Authorization: SessionId
      },
      data: {
        user_actions
      }
    })
      .then(res => {
        if (Array.isArray(res.data.processed))
          commit("setProcessedList", res.data.processed);
        else commit("setProcessedList", []);
        return res;
      })
      .catch(err => err);
  },
  async createRequest(
    { commit },
    { requestForm, user_actions, SessionId, U_CREATED_BY }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/api/bp_requests/create`,
      headers: {
        Authorization: SessionId
      },
      data: {
        U_CREATED_BY,
        ...requestForm,
        user_actions
      }
    })
      .then(res => {
        commit("addPending", res.data.result);
        return res;
      })
      .catch(err => err);
  },

  async editRequest(
    { commit },
    { requestForm, user_actions, SessionId, U_UPDATED_BY }
  ) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/api/bp_requests/post/${requestForm.DocEntry}`,
      headers: {
        Authorization: SessionId
      },
      data: {
        user_actions,
        U_UPDATED_BY,
        ...requestForm
      }
    })
      .then(async res => {
        if (res.status === 204)
          commit("editPending", { ...requestForm, U_STATUS: "Requested" });

        return res;
      })
      .catch(err => err);
  },

  async processRequest(
    { commit },
    { requestForm, user_actions, SessionId, U_UPDATED_BY }
  ) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/api/bp_requests/process/${requestForm.DocEntry}`,
      headers: {
        Authorization: SessionId
      },
      data: {
        user_actions,
        U_UPDATED_BY,
        ...requestForm
      }
    })
      .then(async res => {
        if (res.status === 204) {
          commit("processRequest", {
            ...requestForm,
            U_DATE_PROCESSED: moment().format("YYYY-MM-DD"),
            U_STATUS: "Processed"
          });
        }
        return res;
      })
      .catch(err => err);
  },
  async fetchApprovedList({ commit }, { SessionId, user_actions }) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/api/bp_requests/approved`,
      headers: {
        Authorization: SessionId
      },
      data: {
        user_actions
      }
    })
      .then(res => {
        if (Array.isArray(res.data.approved))
          commit("setApprovedList", res.data.approved);
        else commit("setApprovedList", []);
        return res;
      })
      .catch(err => err);
  },

  async approveRequest(
    { commit },
    { requestForm, user_actions, SessionId, U_UPDATED_BY }
  ) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/api/bp_requests/approve/${requestForm.DocEntry}`,
      headers: {
        Authorization: SessionId
      },
      data: {
        user_actions,
        U_UPDATED_BY,
        ...requestForm
      }
    })
      .then(async res => {
        if (res.status === 204)
          commit("approveRequest", { ...requestForm, U_STATUS: "Approved" });
        return res;
      })
      .catch(err => err);
  }
};
