import axios from "axios";

export default {
  async fetchSupplierList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/suppliers/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.suppliers))
          commit("setSupplierList", res.data.suppliers);
        else commit("setSupplierList", []);
        return res;
      })
      .catch(err => err);
  },
};
