import axios from "axios";

export default {
  async fetchCustomerList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/customers/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.customers))
          commit("setCustomerList", res.data.customers);
        else commit("setCustomerList", []);
        return res;
      })
      .catch(err => err);
  }
};
