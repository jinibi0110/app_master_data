import axios from "axios";

export default {
  async fetchStateList({ commit }, { CompanyDB, Country, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/states/`,
      headers: {
        Authorization: SessionId
      },
      params: {
        CompanyDB,
        Country
      }
    })
      .then(res => {
        if (Array.isArray(res.data.states))
          commit("setStateList", res.data.states);
        else commit("setStateList", []);
        return res;
      })
      .catch(err => err);
  }
};
