import axios from "axios";

export default {
  async fetchCurrencyList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/currencies/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.currencies))
          commit("setCurrencyList", res.data.currencies);
        else commit("setCurrencyList", []);
        return res;
      })
      .catch(err => err);
  }
};
