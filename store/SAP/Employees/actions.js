import axios from "axios";

export default {
  async fetchEmployeeList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/employees/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.employees))
          commit("setEmployeeList", res.data.employees);
        else commit("setEmployeeList", []);
        return res;
      })
      .catch(err => err);
  }
};
