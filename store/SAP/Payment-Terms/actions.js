import axios from "axios";

export default {
  async fetchPaymentTermList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/payment_terms/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.payment_terms))
          commit("setPaymentTermList", res.data.payment_terms);
        else commit("setPaymentTermList", []);
        return res;
      })
      .catch(err => err);
  }
};
