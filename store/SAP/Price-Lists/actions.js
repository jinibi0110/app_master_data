import axios from "axios";

export default {
  async fetchPriceList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/price_list/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.price_list))
          commit("setPriceList", res.data.price_list);
        else commit("setPriceList", []);
        return res;
      })
      .catch(err => err);
  }
};
