import axios from "axios";

export default {
  async fetchSalesEmployeeList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/sales_employees/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.sales_employees))
          commit("setSalesEmployeeList", res.data.sales_employees);
        else commit("setSalesEmployeeList", []);
        return res;
      })
      .catch(err => err);
  }
};
