import axios from "axios";

export default {
  async fetchCountryList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/countries/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.countries))
          commit("setCountryList", res.data.countries);
        else commit("setCountryList", []);
        return res;
      })
      .catch(err => err);
  }
};
