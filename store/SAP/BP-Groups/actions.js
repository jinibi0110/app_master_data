import axios from "axios";

export default {
  async fetchBPGroupList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/bp_groups/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.bp_groups))
          commit("setBPGroupList", res.data.bp_groups);
        else commit("setBPGroupList", []);
        return res;
      })
      .catch(err => err);
  }
};
