import axios from "axios";

export default {
  async fetchChartAccountList({ commit }, { CompanyDB, SessionId }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/chart_of_accounts/${CompanyDB}`,
      headers: {
        Authorization: SessionId
      }
    })
      .then(res => {
        if (Array.isArray(res.data.chart_of_accounts))
          commit("setChartAccountList", res.data.chart_of_accounts);
        else commit("setChartAccountList", []);
        return res;
      })
      .catch(err => err);
  }
};
