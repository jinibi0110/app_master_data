import axios from "axios";
export default {
  async fetchSignatories({ commit }, { SessionId, user_actions }) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/api/admin/signatories`,
      headers: { Authorization: SessionId },
      data: {
        user_actions
      }
    }).then(res => {
      commit("fetchSignatory", res.data.signatories);

      return res;
    });
  },

  async addSignatory(
    { commit },
    { signatoryData, signatoryDetails, U_CREATED_BY, user_actions, SessionId }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/api/admin/signatories/add_signatory`,
      headers: { Authorization: SessionId },
      data: {
        ...signatoryData,
        U_CREATED_BY,
        user_actions
      }
    }).then(res => {
      commit("addSignatory", { ...res.data.results, ...signatoryDetails });

      return res;
    });
  },

  async editSignatory(
    { commit },
    { signatoryData, signatoryDetails, U_UPDATED_BY, user_actions, SessionId }
  ) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/api/admin/signatories/edit_signatory/${signatoryData.Code}`,
      headers: { Authorization: SessionId },
      data: {
        ...signatoryData,
        U_UPDATED_BY,
        user_actions
      }
    }).then(res => {
      commit("editSignatory", { ...signatoryData, ...signatoryDetails });

      return res;
    });
  },

  async fetchSignatoriesByCompany({ commit }, { SessionId, user_actions }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/api/admin/signatories/`,
      headers: { Authorization: SessionId },
      data: {
        user_actions
      }
    }).then(res => {
      commit("fetchSignatory", res.data.signatories);

      return res;
    });
  }
};
