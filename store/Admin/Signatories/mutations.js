export default {
  fetchSignatory(state, data) {
    state.signatoriesList = data;
  },
  addSignatory(state, signatoryData) {
    state.signatoriesList.unshift(signatoryData);
  },

  editSignatory(state, signatoryData) {
    
    const index = state.signatoriesList.findIndex(
      signatory => signatory.Code == signatoryData.Code
    );

    state.signatoriesList.splice(index, 1);
    state.signatoriesList.push(signatoryData);
  }
};
